import logging
import re

import discord
import yaml

# TODO: 
# Check if new ~add command already exists
# Deal with spoilers


# Read config file
with open("config.yaml", "r") as yamlfile:
    config = yaml.load(yamlfile, Loader=yaml.Loader)

# Setup logging
logging.basicConfig(level=logging.INFO)


class MyClient(discord.Client):
    def build_commands(self, return_string):
        # Read lore file
        with open("lore.yaml", "r") as yamlfile:
            lore = yaml.load(yamlfile, Loader=yaml.Loader)
        # Initialize available commands list
        avail_commands = []
        avail_commands_str = f'{config["command_prefix"]}help\n'
        # Fetch all known lore commands and
        lore_keys = lore.keys()

        # Build available commands list
        for x in lore_keys:
            avail_commands.append(x)
            avail_commands_str += f'{config["command_prefix"]}{x}\n'
        if return_string is True:
            return avail_commands_str
        else:
            return avail_commands

    def find_num_values(self):
        # Read lore file
        with open("lore.yaml", "r") as yamlfile:
            lore = yaml.load(yamlfile, Loader=yaml.Loader)
        # Find how many values are in each key
        # https://stackoverflow.com/questions/51107992/counting-the-number-of-values-in-a-specific-key-for-a-python-dictionary
        values_count = []
        for key, value in lore.items():
            values_count.append(sum(1 for v in value if v))
        return values_count

    async def on_ready(self):
        print('We have logged in as {0.user}'.format(client))

    async def on_message(self, message):
        # Ignore message if the bot was the sender
        if message.author.id == self.user.id:
            return

        if message.content.startswith(f'{config["command_prefix"]}'):
            pass
        else:
            return

        # Check to make sure the message is sent from the command user
        def check(user):
            return user.author == message.author

        # Respond to ~add command
        if message.content.startswith(f'{config["command_prefix"]}add'):
            # Check to make sure the right role is adding lore
            if str(message.author) not in config["authorized_add_users"]:
                await message.channel.send(
                    "You're not authorized to use that command.")
                return

            if message.content == f'{config["command_prefix"]}add':
                await message.channel.send("Name of new lore command?")
                lore_command = await self.wait_for("message", check=check)
                lore_command = lore_command.content.lower()
            else:
                # Remove ~add using regex
                lore_command = re.sub(r'^\W*\w+\W*', '', message.content.lower())

            # spoiler = False
            # await message.channel.send("Is this a spoiler?")
            # spoiler_response = await self.wait_for("message")

            # if "y" in spoiler_response.content.lower(
            # ) or "yes" in spoiler_response.content.lower():
            #     spoiler = True
            #     await message.channel.send("Which expac?")
            #     spoiler_xpac = self.wait_for("message", check=check)

            await message.channel.send(
                "Ready to receive new lore. When finished, send the ~fin command."
            )
            # Open yaml lore file
            with open("lore.yaml", "r") as yamlfile:
                lore = yaml.full_load(yamlfile)
                #lore = yaml.full_load(yamlfile)
            # New lore dictionary
            lore_d = {lore_command: {}}
            # Counter for number of messages sent
            i = 1
            while True:
                lore_message = await self.wait_for("message", check=check)

                # If ~fin command, stop waiting for new messages
                if lore_message.content.startswith(
                        f'{config["command_prefix"]}fin'):
                    try:
                        # Add new lore to yaml file
                        lore.update(lore_d)
                        with open(r'lore.yaml', 'w') as yamlfile:
                            yaml.dump(lore,
                                      yamlfile,
                                      default_style='',
                                      indent=4)
                        await message.channel.send(
                            f'Stored! The new command is {config["command_prefix"]}{lore_command}'
                        )
                    except Exception as e:
                        await message.channel.send(f'There was an error: {e}')
                        print(f'Exception while storing new lore: {e}')

                    return
                else:
                    # Append to yml
                    lore_d[lore_command].update(
                        {i: lore_message.content})
                    i += 1

        # Respond to ~del command
        if message.content.startswith(f'{config["command_prefix"]}del'):
            if message.content == f'{config["command_prefix"]}del':
                # Send embed reply
                embedVar = discord.Embed(color=0x5a9ef4)
                embedVar.add_field(name="Which command do you want to delete?",
                                   value=self.build_commands(True),
                                   inline=False)

                await message.reply(embed=embedVar, mention_author=False)

                command_to_del = await self.wait_for("message", check=check)
                command_to_del = command_to_del.content.lower()
            else:
                command_to_del = message.content.split()[-1].lower()

            # Delete from yaml
            with open("lore.yaml", "r") as yamlfile:
                lore = yaml.load(yamlfile, Loader=yaml.Loader)
            del lore[command_to_del]
            try:
                # Write to yaml file
                with open(r"lore.yaml", "w") as yamlfile:
                    yaml.dump(lore, yamlfile, default_style='', indent=4)
                await message.channel.send(
                    f'Deleted {config["command_prefix"]}{command_to_del}!')
            except Exception as e:
                await message.channel.send(f'There was an error: {e}')
                print(f'Exception while deleting command: {e}')

        # Respond to ~help command
        if message.content.startswith(f'{config["command_prefix"]}help'):
            # Send embed reply
            embedVar = discord.Embed(color=0x5a9ef4)
            embedVar.add_field(name="Available Commands",
                               value=self.build_commands(True),
                               inline=False)
            await message.reply(embed=embedVar, mention_author=False)

        # Respond to commands
        avail_commands = self.build_commands(False)

        n = 1
        # Open yaml lore file
        with open("lore.yaml", "r") as yamlfile:
            lore = yaml.load(yamlfile, Loader=yaml.Loader)
        for enum, x in enumerate(avail_commands):
            command = x

            if message.content.startswith(
                    f'{config["command_prefix"]}{command}'):
                # Send message(s). Multiple if > 1 values in key
                for v in range(0, self.find_num_values()[enum]):
                    await message.channel.send(lore[avail_commands[enum]][n])

                    n += 1
                return


client = MyClient()
client.run(config["token"])
