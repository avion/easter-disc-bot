# Easter Discord Bot

Getting set up:  
1) Copy samply.config.yml and rename it to config.yml
2) Go to https://discord.com/developers/applications and create a bot. Copy the bot token and put it in config.yml
3) On the Discord bot website, click OAuth2, URL Generator
4) Under Scopes, check "bot". Under Bot permissions, check Send Messages and Read Message History
5) Copy the generated URL at the bottom and add the bot to your server
6) `python -m venv venv`
7) `pip install -r requirements.txt`   
8) `python main.py`
